# Create Role
aws iam create-role --role-name lambda-ex --assume-role-policy-document file://trust-policy.json

# Add permissions to the role
aws iam attach-role-policy --role-name lambda-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

# Create the deployment
zip function.zip index.js

# Create the lambda Function
aws lambda create-function --function-name my-function \
--zip-file fileb://function.zip --handler index.handler --runtime nodejs12.x \
--role arn:aws:iam::920190234064:role/lambda-ex

# Get logs for an invocation
aws lambda invoke --function-name my-function out --log-type Tail

# Use Base64 to decode logs
aws lambda invoke --function-name my-function out --log-type Tail \
--query 'LogResult' --output text |  base64 -d

# Clean All
aws lambda delete-function --function-name my-function
aws iam delete-role --role-name lambda-ex
aws iam detach-role-policy --role-name lambda-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

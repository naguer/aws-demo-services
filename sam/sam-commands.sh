# Step 1 - Download a sample application
#  For this tutorial, we recommend that you choose AWS Quick Start Templates,
# the Zip package type, the runtime of your choice, and the Hello World Example.
sam init
three

# Step 2 - Build your application
cd sam-app
sam build
ls ../.aws-sam

# Step 3 - Deploy your application
sam deploy --guided

# This command deploys your application to the AWS Cloud. It takes the deployment artifacts that you build with the sam build command, packages and uploads them to an Amazon Simple Storage Service (Amazon S3) bucket that the AWS SAM CLI creates, and deploys the application using AWS CloudFormation. In the output of the sam deploy command, you can see the changes being made to your AWS CloudFormation stack.

If your application created an HTTP endpoint, the outputs that sam deploy generates also show you the endpoint URL for your test application. You can use curl to send a request to your application using that endpoint URL. For example:

curl https://<restapiid>.execute-api.us-east-1.amazonaws.com/Prod/hello/

# Test local
sam local start-api
curl http://127.0.0.1:3000/hello

# Clean
aws cloudformation delete-stack --stack-name sam-app --region us-east-1

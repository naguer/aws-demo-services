# Create cluster
eksctl create cluster -f eks-cluster.yaml

# Pods
kubectl run nginx --image=nginx
kubectl port-forward nginx 8080:80
kubectl exec -ti nginx -- /bin/bash
echo "AWS Course" > /usr/share/nginx/html/index.html
kubectl port-forward nginx 8080:80
kubectl delete pod nginx 

# Deployment
kubectl create deployment --image=nginx nginx --replicas=2
kubectl get all 
kubectl delete pod nginx-* 
kubectl scale deploy nginx --replicas=3
kubectl get deploy nginx -n webinar-k8s

# Service
kubectl expose deployment nginx --port=80 --name nginx-service --type=LoadBalancer 

# HPA
kubectl edit deploy nginx 
# add requests: cpu: 0,01 
kubectl autoscale deployment nginx --cpu-percent=30 --min=1 --max=5
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://aa3ea56c6a5c84c1caa58da74f5670f6-160694838.us-east-1.elb.amazonaws.com; done"


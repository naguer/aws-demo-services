packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.6"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "amazonlinux" {
  ami_name      = "aws_devops"
  instance_type = "t3.medium"
  region        = "us-east-1"
  source_ami    = "ami-0b5eea76982371e91"
  ssh_username  = "ec2-user"
}

build {
  name    = "aws-devops"
  sources = ["source.amazon-ebs.amazonlinux"]
  provisioner "shell" {
    script = "bootstrap.sh"
  }
}


